@isTest
public class ContractClauseControllerTest {
    static testMethod void myUnitTest() {
        Account Accnt = new Account(Name='Test FName');
        insert Accnt;
        Contract contract1 = new Contract(AccountId=Accnt.Id,Status='Draft',ContractTerm = 12);
        Contract_Clause__c clause = new Contract_Clause__c(Name='test contract name',Type__c='Other');
        insert contract1;
        insert clause;
        Contract_Clause_Junction__c junc = new Contract_Clause_Junction__c(Contract__c=contract1.AccountId, Contract_Clause__c = clause.Id);
        junc.Name = 'test junction name';
        junc.Contract__c = contract1.Id;
        junc.Contract_Clause__c = clause.Id;
        upsert junc;
        ApexPages.StandardController sc = new ApexPages.StandardController(contract1);
        ContractClauseController controller = new ContractClauseController(sc);        
        controller.juncObj = junc;
        controller.contract = contract1;
        test.startTest();
            controller.setId();
            controller.showPopup();
            controller.closePopup();
            controller.getItems();
            controller.save();
        	PageReference testPage = new pagereference('/apex/ContractClauseDisplay');
            Apexpages.currentPage().getParameters().put('clauseId',junc.Id);
            controller.deleteRecord();
            PageReference testPage2 = new pagereference('/apex/contractDisplay');
            Apexpages.currentPage().getParameters().put('contractId',contract1.Id);
            controller.getContract();
            controller.getRelatedList();
        test.stopTest();
    }
    
}