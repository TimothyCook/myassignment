public class ContractClauseController {
    public String Record {get; set;}
    public List<Contract> contractList = new List<Contract>();
    public Contract contract = new Contract();
    Public List<Contract_Clause_Junction__c> cList = new List<Contract_Clause_Junction__c>();
    public String clauseId {get; set;}
    public Boolean displayPopup {get;set;}
    public Contract_Clause_Junction__c juncObj {get; set;}
    //constructor for controller extension
    public ContractClauseController(ApexPages.StandardController controller){
		juncObj = new Contract_Clause_Junction__c();
   }
	//returns a list of all contracts stored in database
    public List<Contract_Clause_Junction__c> getRelatedList(){
         cList = Database.query('SELECT Name, Contract__c, Contract_Clause__c FROM Contract_Clause_Junction__c WHERE Contract__r.Id = \''+Record+'\'');
         return cList;
    }
    //returns a specific contract using its Id
    public Contract getContract(){
        Record = apexpages.currentPage().getParameters().get('contractId');
        contract = Database.query(
        'SELECT ContractNumber, Account.Name, StartDate, EndDate, ContractTerm FROM Contract WHERE Id = \'' + Record + '\''
        );
        return contract;
    }
    //Deletes a clause from a contract using its Id
    public void deleteRecord(){
        clauseId = apexpages.currentPage().getParameters().get('clauseId');
        Contract_Clause_Junction__c cl = Database.query('SELECT name FROM Contract_Clause_Junction__c WHERE Id = \''+clauseId+'\'');
        delete cl;
    }
    //Sets contract Id to variable
    public void setId(){
        Record = apexpages.currentPage().getParameters().get('contractId');
        
    }
	//opens popup window
    public void showPopup()
    {
	    displayPopup = true;
    }
	//closes popup window
    public void closePopup() {

        displayPopup = false;
	}
    //saves / upserts a Contract_Clause_junction object
    public void save(){
        upsert juncObj; 
        closePopup();
        juncObj = new Contract_Clause_Junction__c();
    }
    //For apex:selectlist, not currently implemented
    public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Financial','Financial'));
            options.add(new SelectOption('Variable Length','Variable Length'));
            options.add(new SelectOption('Other','Other'));
            return options;
    }
}